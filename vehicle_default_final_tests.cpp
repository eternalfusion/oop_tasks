﻿#include "test_functions_definitions.h"
#include <iostream>
#include <variant>
#include <vector>
#include "BuildAttributes.h"
#include "Vehicle.h"
#include "VehicleBuilder.h"


void execute_vehicle_final_tests() {
    Car car("test car");

    auto qual_a_builder = new QualitativeAttributeBuilder;
    auto quan_a_builder = new QuantitativeAttributeBuilder;

    auto carColor = qual_a_builder->setAttributeName("Color")->setAttributeRange("Red", "Blue", "Green")->build();

    car.attributes.push_back(carColor);
    car.features = {"Air conditioning", "Power windows"};

    Motorcycle motorcycle("Motorcycle");
    auto motoDispl = quan_a_builder->setAttributeName("Engine Displacement")->setAttributeRange(250, 500)->build();

    motorcycle.attributes.push_back(motoDispl);
    motorcycle.features = {"Kick start", "Disc brakes"};

    Vehicle* clonedCar = car.clone();
    auto carSpeed = quan_a_builder->setAttributeName("Speed")->setAttributeRange(0, 200)->build();
    clonedCar->attributes.push_back(carSpeed);
    
    Vehicle* clonedMotorcycle = motorcycle.clone();

    std::cout << "Original Car:" << std::endl;
    car.printDetails();
    std::cout << std::endl;

    std::cout << "Cloned Car:" << std::endl;
    clonedCar->printDetails();
    std::cout << std::endl;

    std::cout << "Original Motorcycle:" << std::endl;
    motorcycle.printDetails();
    std::cout << std::endl;

    std::cout << "Cloned Motorcycle:" << std::endl;
    clonedMotorcycle->printDetails();
    std::cout << std::endl;
}
