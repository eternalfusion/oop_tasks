#include "test_functions_definitions.h"

int main(int argc, char* argv[])
{
    execute_attribute_builder_tests(); // builder pattern for attributes
    execute_attribute_prototype_pattern_tests(); // prototype (clone) pattern for attributes
    //execute_vehicle_final_tests();
    execute_vehicle_builder_final_tests(); // builder + prototype pattern for vehicles using previous Attribute derived classes
    
    return 0;
}
