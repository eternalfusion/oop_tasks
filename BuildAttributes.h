﻿#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <variant>
#include "Attribute.h"

class QuantitativeAttributeBuilder {
protected:
    QuantitativeAttribute* attribute = new QuantitativeAttribute();
public:
    QuantitativeAttributeBuilder() = default;
    virtual ~QuantitativeAttributeBuilder() { delete attribute; }

    QuantitativeAttributeBuilder* setAttributeName(const std::string& name)
    {
        attribute->name = name;
        return this;
    }
    
    QuantitativeAttributeBuilder* setAttributeRange(float min, float max)
    {
        attribute->min = min;
        attribute->max = max;
        return this; 
    }
    Attribute* build()
    {
        Attribute* tmpAttribute = attribute;
        attribute = new QuantitativeAttribute();
        return tmpAttribute;
    }
};

class QualitativeAttributeBuilder {
protected:
    QualitativeAttribute* attribute = new QualitativeAttribute();
public:
    QualitativeAttributeBuilder() = default;
    virtual ~QualitativeAttributeBuilder() { delete attribute; }

    QualitativeAttributeBuilder* setAttributeName(const std::string& name)
    {
        attribute->name = name;
        return this;
    }
    template <typename ...T>
    QualitativeAttributeBuilder* setAttributeRange(T... range)
    {
        attribute->values = {range ...};
        return this; 
    }
    Attribute* build()
    {
        Attribute* tmpAttribute = attribute;
        attribute = new QualitativeAttribute();
        return tmpAttribute;
    }
};