#include "test_functions_definitions.h"
#include "BuildAttributes.h"


void execute_attribute_builder_tests() {
    auto builder1 = new QuantitativeAttributeBuilder();
    auto speed = builder1->setAttributeName("Speed")->setAttributeRange(0, 200)->build();
    speed->print();
    
    auto builder2 = new QualitativeAttributeBuilder();
    auto color = builder2->setAttributeName("Color")->setAttributeRange("Red", "Blue", "Green")->build();
    color->print();
}