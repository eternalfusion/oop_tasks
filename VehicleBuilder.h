﻿#pragma once
#include "Vehicle.h"

/*class VehicleBuilder
{
public:
    
};*/


class Car : public Vehicle {
public:
    Car() = default;
    Car(std::string n)
    {
        name = n;
    }
    
    Vehicle* clone() override {
        return new Car(*this);
    }
};

class Motorcycle : public Vehicle {
public:
    Motorcycle() = default;
    Motorcycle(std::string n)
    {
        name = n;
    }
    Vehicle* clone() override {
        return new Motorcycle(*this);
    }
};

template <class T>
concept IsVehicle = std::is_base_of_v<Vehicle, T>; // новая фича из c++ 20 для проверки типов в IDE.

template <IsVehicle VehicleType>
class VehicleBuilder {
public:
    VehicleType* vehicle_ = new VehicleType();
    VehicleBuilder() = default;
    VehicleBuilder* setVehicleName(const std::string& name)
    {
        vehicle_->name = name;
        return this;
    }
    VehicleBuilder* setVehicleAttributeList(const std::vector<Attribute*>& attrs)
    {
        vehicle_->attributes = attrs;
        return this;
    }
    VehicleBuilder* setVehicleFeaturesList(const std::vector<std::string>& features)
    {
        vehicle_->features = features;
        return this;
    }
    VehicleType* build()
    {
        auto tmpVehicle = vehicle_;
        vehicle_ = new VehicleType();
        return tmpVehicle;
    }
};
