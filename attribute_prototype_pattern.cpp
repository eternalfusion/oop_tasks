#include "test_functions_definitions.h"
#include <vector>
#include "Attribute.h"

void execute_attribute_prototype_pattern_tests() {
    QuantitativeAttribute q_attr("Speed", 0.0, 200.0);
    QualitativeAttribute ql_attr("Color", {"Red", "Blue", "Green"});

    Attribute* q_attr_clone = q_attr.clone();
    Attribute* ql_attr_clone = ql_attr.clone();

    q_attr_clone->print();
    ql_attr_clone->print();
}