﻿#pragma once
#include <iostream>
#include <string>

class Attribute {
public:
    Attribute() {}
    virtual Attribute* clone() const = 0;
    virtual void print() const = 0;
    ~Attribute() = default;
};

class QuantitativeAttribute : public Attribute {
public:
    std::string name;
    float min;
    float max;
    QuantitativeAttribute() = default;
    QuantitativeAttribute(std::string n, float min_val, float max_val) 
        : name(n), min(min_val), max(max_val) {}

    QuantitativeAttribute* clone() const override {
        return new QuantitativeAttribute(*this);
    }

    void print() const override {
        std::cout << "Quantitative Attribute: " << name << ", Range: [" 
                  << min << ", " << max << "]" << std::endl;
    }
};

class QualitativeAttribute : public Attribute {
public:
    std::string name;
    std::vector<std::string> values;
    QualitativeAttribute() {}
    QualitativeAttribute(std::string n, std::vector<std::string> vals) 
        : name(n), values(vals) {}

    QualitativeAttribute* clone() const override {
        return new QualitativeAttribute(*this);
    }

    void print() const override {
        std::cout << "Qualitative Attribute: " << name << ", Values: ";
        for (const auto& val : values) {
            std::cout << val << " ";
        }
        std::cout << std::endl;
    }
};