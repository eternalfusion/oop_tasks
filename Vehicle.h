﻿#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Attribute.h"

class Vehicle {
public:
    std::string name;
    std::vector<Attribute*> attributes;
    std::vector<std::string> features;

    virtual Vehicle* clone() = 0;

    void printDetails() {
        std::cout << "Name: " << name << std::endl;
        std::cout << "Attributes:" << std::endl;
        for (const auto& attribute : attributes) {
            attribute->print();
            std::cout << std::endl;
        }
        std::cout << "Features:" << std::endl;
        for (const auto& feature : features) {
            std::cout << "  - " << feature << std::endl;
        }
    }
};
