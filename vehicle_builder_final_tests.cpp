#include "test_functions_definitions.h"
#include <iostream>
#include <variant>
#include <vector>
#include "BuildAttributes.h"
#include "Vehicle.h"
#include "VehicleBuilder.h"


void execute_vehicle_builder_final_tests() {
    auto carBuilder = VehicleBuilder<Car>();
    auto motoBuilder = VehicleBuilder<Motorcycle>();

    auto qual_a_builder = new QualitativeAttributeBuilder;
    auto quan_a_builder = new QuantitativeAttributeBuilder;

    

    std::vector<Attribute*> attributesCar;
    auto carColor = qual_a_builder->setAttributeName("Color")->setAttributeRange("Red", "Blue", "Green")->build();
    attributesCar.push_back(carColor);
    auto car = carBuilder.setVehicleName("test car")->setVehicleAttributeList(attributesCar)->setVehicleFeaturesList({"Air conditioning", "Power windows"})->build();


    std::vector<Attribute*> attributesMotorcycle;
    auto motoColor = qual_a_builder->setAttributeName("Color")->setAttributeRange("Red", "Blue", "Green")->build();
    auto motoDispl = quan_a_builder->setAttributeName("Engine Displacement")->setAttributeRange(250, 500)->build();
    attributesMotorcycle.push_back(motoColor);
    attributesMotorcycle.push_back(motoDispl);
    auto motorcycle = motoBuilder.setVehicleName("Motorcycle")->setVehicleAttributeList(attributesCar)->setVehicleFeaturesList({"Kick start", "Disc brakes"})->build();


    Vehicle* clonedCar = car->clone();
    auto carSpeed = quan_a_builder->setAttributeName("Speed")->setAttributeRange(0, 200)->build();
    clonedCar->attributes.push_back(carSpeed);
    
    Vehicle* clonedMotorcycle = motorcycle->clone();

    std::cout << "Original Car:" << std::endl;
    car->printDetails();
    std::cout << std::endl;

    std::cout << "Cloned Car:" << std::endl;
    clonedCar->printDetails();
    std::cout << std::endl;

    std::cout << "Original Motorcycle:" << std::endl;
    motorcycle->printDetails();
    std::cout << std::endl;

    std::cout << "Cloned Motorcycle:" << std::endl;
    clonedMotorcycle->printDetails();
    std::cout << std::endl;
}
